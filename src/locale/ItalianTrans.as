package locale
{
	public class ItalianTrans
	{
		public static var str1:String = "ciao <name> grazie.";
		[Bindable]
		public static var str2:String = "ciao <name> grazie per il commento.";
		[Bindable]
		public static var str3:String = "<name> grazie per il commento.";
		[Bindable]
		public static var str4:String = "grazie per il commento.";
		[Bindable]
		public static var str5:String = "";
		[Bindable]
		public static var str6:String = "";
		[Bindable]
		public static var str7:String = "";
		[Bindable]
		public static var str8:String = ""; 
	}
}