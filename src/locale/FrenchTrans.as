package locale
{
	public class FrenchTrans
	{
		public static var str1:String = "salut <name> merci.";
		[Bindable]
		public static var str2:String = "salut <name> je te remercie.";
		[Bindable]
		public static var str3:String = "salut <name> merci pour le commentaire.";
		[Bindable]
		public static var str4:String = "<name> merci pour le commentaire.";
		[Bindable]
		public static var str5:String = "merci pour le commentaire.";
		[Bindable]
		public static var str6:String = "bonjour <name> merci pour le commentaire.";
		[Bindable]
		public static var str7:String = "bonjour <name> merci.";
		[Bindable]
		public static var str8:String = "bonjour <name> je vous remercie."; 
	}
}