package events
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	public class ConfigureFanPagesEvent extends Event
	{
		public var fanPages:ArrayCollection;
		
		public static const FAN_PAGE_SELECTED:String = "FAN_PAGE_SELECTED";
		
		public function ConfigureFanPagesEvent(type:String, fanpages:ArrayCollection, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.fanPages = fanpages;
		}
	}
}