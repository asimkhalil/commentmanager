package com.pageone.flickr.frame {

	public dynamic class FlickrResult {
		public function FlickrResult() {
		}

		public var id:String, title:String, owner:String, secret:String, server:String, farm:String, owner_name:String, licenseId:Number;
		
		public function get owner_url():String {
			return "http://flickr.com/" + owner;
		}
		
		/*s small square 75x75
		q large square 150x150
		t thumbnail, 100 on longest side
		m small, 240 on longest side
		n small, 320 on longest side
		- medium, 500 on longest side
		z medium 640, 640 on longest side
		c medium 800, 800 on longest side†
		b large, 1024 on longest side*
		o original image, either a jpg, gif or png, depending on source format*/
		
		private static const imageSiffixes:Array=["_s", "_q", "_t", "_m", "_n", "", "_z", "_c", "_b", "_o"];
		public function getLargestUrl():String {
			for (var i:int=imageSiffixes.length-1; i>=0; i--) {
				if(this.hasOwnProperty("url" + imageSiffixes[i])) {
					return this["url" + imageSiffixes[i]];
				}
			}
			return "";
		}

	}
}
