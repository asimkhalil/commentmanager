package com.pageone.flickr.events
{
	import flash.events.Event;
	
	public class FlickrCallFailedEvent extends Event
	{
		public function FlickrCallFailedEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public var reason:String;
		
		public static const flickrCallFailed:String="flickrCallFailed";
	}
}