package com.pageone.settings
{
	import com.pageone.framework.MCBlog;
	
	import flash.data.EncryptedLocalStore;
	import flash.desktop.NativeApplication;
	import flash.display.DisplayObject;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;

	[Bindable]
	public class Settings
	{
		public function Settings()
		{
			if(!_instance) {
				_instance=this;
			}
		}
		
		public static function getAppVersion():String {
			var appXml:XML=NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace=appXml.namespace();
			var appVersion:String=appXml.ns::versionNumber[0];
			return appVersion;
		}
		
		private static var _instance:Settings;

		public var prefsFile:File=File.applicationStorageDirectory.resolvePath("prefs.ini");
		
		public var valid:Boolean;
		public var prodid:int;
		
		public var application:DisplayObject;
		
		public static function get app():Settings
		{
			if(_instance==null) new Settings();
			return _instance;
		}

		
		public function read():void {
			if(!prefsFile.exists) {
				save();
			}
			
			var fs:FileStream=new FileStream();
			fs.open(prefsFile, FileMode.READ);
			var s:Array=fs.readUTFBytes(fs.bytesAvailable).split("\r\n");
			fs.close();
			
			var dirty:Boolean=false;
			
			for each(var line:String in s) {
				try {
					var obj:Array=line.split("=", 2);
					if (this[obj[0]] is Number) {
						this[obj[0]] = Number(obj[1]);
					} else if (this[obj[0]] is Boolean) {
						this[obj[0]] = (obj[1] == "true");
					} else {
						this[obj[0]] = obj[1];
					}
				} catch(err:Error) {
					dirty=true;
				}
			}
			
			if(dirty) save();
			
			readBlogs();
		}
		
		public function save():void {
			var s:Array=[];
			for each(var key:String in prefsFields) {
				try {
					s.push(key + "=" + this[key]);
				} catch(err:Error) {
					
				}
			}
		
		
			var fs:FileStream=new FileStream();
			fs.open(prefsFile, FileMode.WRITE);
			fs.writeUTFBytes(s.join("\r\n"));
			fs.close();
		}

		
		public var license:String="";
		
		public var mac:String="";
		
		public var prefsFields:Array=["license","mac"];
		
		public var blogs:ArrayCollection;
		
		public function readBlogs():void {
			blogs=new ArrayCollection();
			var ba:ByteArray=EncryptedLocalStore.getItem("blogs");
			if(ba != null) {
				ba.inflate();
				ba.position=0;
				var s:String=ba.readUTFBytes(ba.bytesAvailable);
				var x:XML=new XML(s);
				
				for each(var blog:XML in x.blog) {
					blogs.addItem(MCBlog.readXML(blog));
				}
			}
		}
		
		public function writeBlogs():void {
			var s:String="<blogs>";
			for each(var b:MCBlog in blogs) {
				s+=b.toXML();
			}
			s+="</blogs>";
			var x:XML=new XML(s);
			var ba:ByteArray=new ByteArray();
			ba.writeUTFBytes(x.toString());
			ba.deflate();
			EncryptedLocalStore.setItem("blogs", ba);
		}
		
	}
}