package com.jayvenka.fb
{
	import com.adobe.utils.DateUtil;
	import com.ak33m.rpc.core.Method;
	import com.facebook.graph.FacebookDesktop;
	import com.facebook.graph.data.FacebookSession;
	import com.facebook.graph.net.FacebookRequest;
	
	import components.FacebookDataFaultPopup;
	import components.ISO8601Util;
	import components.QuickDateFormatter;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.events.*;
	import flash.filesystem.File;
	import flash.geom.Matrix;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	import model.ApplicationModel;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.events.*;
	import mx.formatters.DateFormatter;
	import mx.managers.CursorManager;
	import mx.managers.PopUpManager;
	import mx.olap.aggregators.CountAggregator;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	
	import org.as3commons.lang.DateUtils;
	
	
	public class FacebookCenter extends EventDispatcher{
		[Bindable]
		public var isLoggedIn:Boolean;
		[Bindable]
		public var myPages:ArrayCollection;
		//Live app key
		private var APP_ID:String 					="1441111016127594"//"244303622442927";
		private var APP_SECRET:String 					=""//"244303622442927";
		//Testing 
		//private const APP_ID:String 					="212830715572925";// "596604607068712";
		private const FACEBOOK_APP_ORIGIN:String 		= "http://avectris.com/";
		private const PERMISSIONS:Array 				= ["manage_pages", "read_insights", "publish_actions", "publish_pages","user_likes"];
		public var accessToken:String 					= "";
		[Bindable] public var facebookPagesObjects:Array;
		[Bindable] public var facebookPagesArray:Array;
		[Bindable] public var postListPageNumber:int = 0;
		[Bindable] public var postListPageSize:int = 16;
		[Bindable] public var postsArray:Array;
		[Bindable] public var postsArrayOrignal:Array;
		[Bindable] public var commentsArray:ArrayCollection;
		[Bindable] public var searchPostsResults:ArrayCollection;
		[Bindable] public var commentsLimit:int = ApplicationModel.batchLimit;
		public function FacebookCenter(appid:String = "", secret:String = "") {
			this.APP_ID = appid;
			this.APP_SECRET = secret;
			this.addEventListener("LoadingCanceledEvent",onLoadingCancelHandler);
			init();
		}
		
		private function onLoadingCancelHandler(event:Event):void {
			FlexGlobals.topLevelApplication.processing = FlexGlobals.topLevelApplication.showProcessingPanel = false;
			if(_posts_urlLoader) {
				_posts_urlLoader.removeEventListener(Event.COMPLETE,dumpPosts);
				_stopProcessingPosts = true;
			}
		}
		
		/* Init facebook connectivity */
		public function init():void {
			FacebookDesktop.init(this.APP_ID, facebookInitResult);
		}
		
		private function facebookInitResult(success:Object, fail:Object):void {
			//if(!success) trace("Init failed") else {trace("init success")login()};
			login();
		}
		
		/* Login to facebook and result*/
		public function login():void {
			logout();
		}
		
		private var loggedinUser:Object;
		
		private function logInFacebookResult(success:Object, fail:Object):void {
			
			if(!success) {
				loggedinUser = success.user;
				trace("Login failed") 
			} else {
				accessToken = success.accessToken;
				grabLongLivedToken(onDone);
				isLoggedIn=true;
				function onDone(token:String):void{
					if(token&&token!="")
						accessToken=token;
					dispatchEvent(new Event("LOGGED_IN"));
					
				}
				//trace("user access token received :" + accessToken);
				var userProfilePhotoUrl:String = FacebookDesktop.getImageUrl(success.uid,"small");
				ApplicationModel.userProfilePhoto = userProfilePhotoUrl;
				if(success.user.first_name) { 
					ApplicationModel.facebookUserName = success.user.first_name+" "+success.user.last_name;
				} else {
					ApplicationModel.facebookUserName = success.user.name;
				}
				ApplicationModel.facebookUserProfileId = success.user.id;
				
			}
		}
		
		public function getProfileImage(uid:String):String {
			return FacebookDesktop.getImageUrl(uid,"small");
		}
		
		private var longLivedTokenResponseObtained:Boolean=false;
		private function grabLongLivedToken(onDone:Function):void{
			var urlLoader:URLLoader=new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE, completeHandler);
			urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, ioErrorHandler);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			var session:FacebookSession=FacebookDesktop.getSession();
			var url:String="http://engagehook.com/app/facebookaccess_commentmaximizer.php?token="+session.accessToken+"&appid="+APP_ID+"&appsecret="+APP_SECRET;
			//var url:String = "https://graph.facebook.com/oauth/access_token?client_id="+APP_ID+"&client_secret="+APP_SECRET+"&grant_type=fb_exchange_token&fb_exchange_token="+session.accessToken
			urlLoader.load(new URLRequest(url));
			function completeHandler(e:Event):void{
				var access_token:String=StringUtil.trim(urlLoader.data.toString());
				//var access_token:String = session.accessToken;
				//ther is an issue where I get the complete event 2 times, first time the token is emptu
				if(access_token==""||longLivedTokenResponseObtained)
					return;
				longLivedTokenResponseObtained=true;
				
				urlLoader.removeEventListener(Event.COMPLETE, completeHandler);
				urlLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, ioErrorHandler);
				urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
				session.accessToken=access_token;
				
				if(onDone!=null)
					onDone(access_token);
			}
			function ioErrorHandler(e:Event):void{
				trace("error getting long lived access token");
				urlLoader.removeEventListener(Event.COMPLETE, completeHandler);
				urlLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, ioErrorHandler);
				urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
				
				onDone(null);
			}
		}
		
		public function getUserPagesList():void {
			var params:Object = {};
			params.access_token = this.accessToken;
			params.limit = 1000;
			FacebookDesktop.api("me/accounts/", getUserPagesCallResult, params);
		}
		
		private var _postbyId_urlLoader:URLLoader;
		
		public function getPostById(pageId:String,postId:String,callback:Function):void {
			var url:String = "https://graph.facebook.com/v2.4/"+postId+"?access_token="+accessToken;
			_postbyId_urlLoader = new URLLoader();
			_postbyId_urlLoader.addEventListener(Event.COMPLETE,callback);
			_postbyId_urlLoader.addEventListener(IOErrorEvent.IO_ERROR,function(event:IOErrorEvent):void {
				FlexGlobals.topLevelApplication.processing = false;
				Alert.show("Could not fetch data from facebook!","Information");
			});
			_postbyId_urlLoader.load(new URLRequest(url));
		}
		
		private var _posts_urlLoader:URLLoader;
		
		private var loadedPosts:ArrayCollection;
		
		private var _from:Date;
		
		private var _to:Date;
		
		private var _stopLoadingNext:Boolean;
		
		public function getAllPosts(pageId:String,from:Date,to:Date):void {
			
			FlexGlobals.topLevelApplication.log.text = "";
			
			_stopProcessingPosts = false;
			
			_from = from;
			
			_to = to;
			
			to = DateUtils.addDays(to,1);
			
			_stopLoadingNext = false;
			
			loadedPosts = new ArrayCollection();
			
			var since:int = QuickDateFormatter.getUnixStampTimeFromDate(from);
			
			var until:int = QuickDateFormatter.getUnixStampTimeFromDate(to);
			
			var url:String = "https://graph.facebook.com/v2.4/"+pageId+"/posts?access_token="+this.accessToken+"&since="+since+"&until="+until+"&limit=100";
			
			FlexGlobals.topLevelApplication.log.text+="Going to call the facebook api for data pages\n";
			
			_posts_urlLoader = new URLLoader();
			_posts_urlLoader.addEventListener(Event.COMPLETE,dumpPosts);
			_posts_urlLoader.load(new URLRequest(url));
			
		}
		
		private var _stopProcessingPosts:Boolean = false;
		
		private var isoUtil:ISO8601Util = new ISO8601Util();
		
		public function dumpPosts(success:Event):void {
			FlexGlobals.topLevelApplication.log.text += "Page retrieved\n";
			if(_stopProcessingPosts) {
				loadedPosts = new ArrayCollection();
				return;
			}
			if(success) {
				(success.target as URLLoader).removeEventListener(Event.COMPLETE,function(event:Event):void{});
				var responseJson:Object = JSON.parse(success.target.data);
				var results:ArrayCollection = new ArrayCollection(responseJson.data as Array);
				if(results.length > 0  && loadedPosts.length < 30) {
					FlexGlobals.topLevelApplication.log.text += "Results found: "+results.length+"\n";
					for each(var post:Object in results) {
						var createdTime:Date = isoUtil.parseDateTimeString(post.created_time);
						createdTime.hours = 0;
						createdTime.minutes = 0;
						createdTime.seconds = 0;
						createdTime.milliseconds = 0;
						if(createdTime >= _from && createdTime <= _to && !(post.type == "status" && post.status_type == undefined)) {
							if(loadedPosts.length < 30) {
								FlexGlobals.topLevelApplication.log.text += "Results added: "+loadedPosts.length+"\n";
								loadedPosts.addItem(post);
							} else {
								getAllPostsResult();
								return;
							}
						}
						if(createdTime < _from) {
							_stopLoadingNext = true;
							getAllPostsResult();
							return;
						}
					}
					FlexGlobals.topLevelApplication.log.text += "Paging call next: "+responseJson.paging+"\n";
					FlexGlobals.topLevelApplication.log.text += "Paging call next: "+responseJson.paging.next+"\n";
					
					if(responseJson.paging != undefined && responseJson.paging.next != undefined) {
						var urlLoader:URLLoader = new URLLoader();
						urlLoader.addEventListener(IOErrorEvent.IO_ERROR,function(event:Event):void {
							getAllPostsResult();
						});
						urlLoader.addEventListener(Event.COMPLETE,function(event:Event):void {
							dumpPosts(event);
						});
						var url:String = responseJson.paging.next;
						//For test
						//url = "https://graph.facebook.com/v2.1/1471556329760828/posts?limit=250&access_token=CAAEuoq0WvVkBAG39g4YpZBh6lEt6S5FmfeVf3mFLF1G7Ulc363wieA9DOYOSvk18yrJUXXkoFxwnvsSn6RVZApKfgZCevbw75vIFULFh0AevangasSZAqelyV6Vo2He92cdFW7XtDGEhNTSwyYCsu0CZA1ckajBimdwXCjNt1rLHhu3Bll6Q5&until=1415808000";
						urlLoader.load(new URLRequest(url));
					}
				} else {
					getAllPostsResult();
				}
			}
		}
		
		public var loadingPostIndex:int = 0;
		
		public function getAllPostsResult():void {
			FlexGlobals.topLevelApplication.log.text += "Combining all pages retrieved\n";
			_posts_urlLoader.removeEventListener(Event.COMPLETE,getAllPostsResult);
			if(loadedPosts) {
				postsArray = new Array();
				postsArrayOrignal = new Array();
				var result:Array = loadedPosts.source;/*JSON.parse(success.target.data).data*/;
				var posts:Array = result as Array;
				if(posts.length == 0) {
					FlexGlobals.topLevelApplication.processing = FlexGlobals.topLevelApplication.showProcessingPanel = false;
					if(FlexGlobals.topLevelApplication.mainView.tab.selectedIndex == 2) {
						Alert.show("There are no recent posts associated with the page selected","Information");
					}
					return;
				}
				var count:int = 0
				postsArray = posts;
				
				if(postsArray.length == 0) {
					FlexGlobals.topLevelApplication.processing = FlexGlobals.topLevelApplication.showProcessingPanel = false;
					if(FlexGlobals.topLevelApplication.mainView.tab.selectedIndex == 2) {
						Alert.show("There are no recent posts associated with the page selected","Information");
					}
					return;
				}
				
				FlexGlobals.topLevelApplication.log.text += "Going to get stats\n";
				
				for(var i:int=0;i<postsArray.length;i++) {
					postsArray[i].created_time = RFC3339toDate(postsArray[i].created_time);
					getStatsForPost(postsArray[i]);
				}
				trace("get status success ");
			} else {
				trace("get status failed");
			}
		}
		
		private function RFC3339toDate( rfc3339:String ):Date
		{
			var datetime:Array = rfc3339.split("T");
			
			var date:Array = datetime[0].split("-");
			var year:int = int(date[0])
			var month:int = int(date[1]-1)
			var day:int = int(date[2])
			
			var time:Array = datetime[1].split(":");
			var hour:int = int(time[0])
			var minute:int = int(time[1])
			var second:Number
			
			// parse offset
			var offsetString:String = time[2];
			var offsetUTC:int
			
			if ( offsetString.charAt(offsetString.length -1) == "Z" )
			{
				// Zulu time indicator
				offsetUTC = 0;
				second = parseFloat( offsetString.slice(0,offsetString.length-1) )
			}
			else
			{
				// split off UTC offset
				var a:Array
				if (offsetString.indexOf("+") != -1)
				{
					a = offsetString.split("+")
					offsetUTC = 1
				}
				else if (offsetString.indexOf("-") != -1)
				{
					a = offsetString.split("-")
					offsetUTC = -1
				}
				else
				{
					throw new Error( "Invalid Format: cannot parse RFC3339 String." )
				}
				
				// set seconds
				second = parseFloat( a[0] )
				
				// parse UTC offset in millisceonds
				var ms:Number = 0
				if ( time[3] )
				{
					ms = parseFloat(a[1]) * 3600000
					ms += parseFloat(time[3]) * 60000
				}
				else
				{
					ms = parseFloat(a[1]) * 60000
				}
				offsetUTC = offsetUTC * ms
			}
			
			return new Date( Date.UTC( year, month, day, hour, minute, second) + offsetUTC );
		}
		
		public function pageResultsSetArray(pResultSet:Array,pageNum:int,pageSize:int):Array {
			var offset:int 		= (pageNum - 1) * pageSize;
			var lastRecord:int 	= (pageNum * pageSize) ;
			var pageResultSet:Array = new Array();
			
			for(var t:int = 0 ; t < pResultSet.length; t++) {
				if (t > lastRecord) break;
				
				if (t >= offset && t < lastRecord) {
					pageResultSet.push(pResultSet[t]);
				}
			}
			
			return pageResultSet;
		}
		
		public function getEngeghookComments(comments:Array):Array {
			var appComments:Array = new Array();
			for each(var comment:Object in comments) {
				if(comment.app_id == APP_ID) {
					var timeStamp:Number = Number(comment.time)*1000;
					var commentDate:Date = new Date();
					commentDate.time = timeStamp;
					var dateDif:int = Math.abs(DateUtils.getHoursDiff(new Date(),commentDate));
					if(dateDif <= 24) 
					{
						appComments.push(comment);
					}
				}
			}
			return appComments;
		}
		
		public function deleteComments(comments:Array):void {
			var count:int = 0;
			var params:Object = {};
			deleteComment();
			function deleteComment():void {
				FacebookDesktop.api("/"+comments[count].id,function(success:Object,fail:Object):void {
					if(success) {
						if(count < comments.length-1) {
							count++;
							deleteComment();
						} else {
							dispatchEvent(new Event("COMMENTS_DELETED_SUCCESSFULLY"));
						}
					} else {
						FlexGlobals.topLevelApplication.processing = true;
					}
				},params,"DELETE");
			}
		}
		
		public function deleteParentComments(pageId:String,postId:String,comments:Array):void {
			var count:int = 0;
			var params:Object = {};
			deleteComment();
			function deleteComment():void {
				FacebookDesktop.api("/"+comments[count].id,function(success:Object,fail:Object):void {
					if(success) {
						if(count < comments.length-1) {
							count++;
							deleteComment();
						} else {
							dispatchEvent(new DataEvent("PARENT_COMMENTS_DELETED_SUCCESSFULLY",true,false,postId));
						}
					}
				},params,"DELETE");
			}
		}
		
		private var urlLoaderCommentCount:URLLoader;
		
		private var urlLoaderCommentCountForBookmarkedPost:URLLoader;
		
		public function populateStatsForPost(post:Object,callback:Function):void {
			var commentCountQuery:String = "http://graph.facebook.com/fql?q=SELECT%20app_id,time,id,fromid,parent_id,text,comment_count%20FROM%20comment%20WHERE%20post_id='"+post.id+"'%20limit%2010000";
			urlLoaderCommentCountForBookmarkedPost = new URLLoader();
			urlLoaderCommentCountForBookmarkedPost.addEventListener(Event.COMPLETE,callback);
			urlLoaderCommentCountForBookmarkedPost.addEventListener(IOErrorEvent.IO_ERROR,function(event:Event):void{
				FlexGlobals.topLevelApplication.processing = false;
				Alert.show("Could not fetch data from facebook!","Information");
			});
			urlLoaderCommentCountForBookmarkedPost.load(new URLRequest(commentCountQuery));
		}
		
		public function getStatsForPost(post:Object):void {
			
			var commentCountQuery:String = "http://graph.facebook.com/fql?q=SELECT%20app_id,time,id,fromid,parent_id,text,comment_count%20FROM%20comment%20WHERE%20post_id='"+post.id+"'%20limit%2010000";			
			//var commentCountQuery:String = "/"+post.id+"/comments";
			//FacebookDesktop.api(commentCountQuery, commentCountResult);
			urlLoaderCommentCount = new URLLoader();
			urlLoaderCommentCount.addEventListener(Event.COMPLETE,commentCountResult);
			urlLoaderCommentCount.load(new URLRequest(commentCountQuery));
			
			var originalComments:ArrayCollection;
			
			var response:ArrayCollection;
			
			function commentCountResult(event:Event):void {
				FlexGlobals.topLevelApplication.log.text += "Stats retrieved ready for calulations\n";
				(event.target as URLLoader).removeEventListener(Event.COMPLETE,commentCountResult)
				var result:Object = JSON.parse(event.target.data);
				var pResponse:Array = result.data;
				if (pResponse) {
					response = new ArrayCollection(pResponse);
					originalComments = new ArrayCollection(pResponse);
					originalComments.filterFunction = filterOriginalComments;
					originalComments.refresh();
					post.commentCount = originalComments.length;//pResponse.length;
					post.appComments = getEngeghookComments(pResponse as Array);
					post.noOfAppComments = post.appComments.length; 
					var col:ArrayCollection = new ArrayCollection(pResponse as Array);
					col.filterFunction = filterUnrepliedComments;
					col.refresh();
					post.unrepliedCommentsCount = col.length;
					dispatchEvent(new DataEvent("UNREPLIED_COMMENTS_FOUND",true,false,col.length.toString()));
					loadingPostIndex++;
					FlexGlobals.topLevelApplication.log.text+="Loaded index: "+loadingPostIndex+" == "+postsArray.length+"\n";
					if(loadingPostIndex == postsArray.length) {
						FlexGlobals.topLevelApplication.log.text+="Going to remove the loading";
						FlexGlobals.topLevelApplication.processing = FlexGlobals.topLevelApplication.showProcessingPanel = false;
						//FlexGlobals.topLevelApplication.mainView.showUnrepliedComments(false);
						loadingPostIndex = 0;
						dispatchEvent(new Event("POSTS_ARRAY_LOADED"));
					}
				} else {
					post.commentCount = 0;
					post.unrepliedCommentsCount = 0;
				}
			}
			
			function filterOriginalComments(item:Object):Boolean {
				var pageId:String = FlexGlobals.topLevelApplication.mainView.dropdown_pages.selectedItem.id
				if(item.parent_id.toString() == "0" && item.fromid != pageId) 
					return true;
				return false;	
			}
			
			function getLatestCommentReplies(item:Object):int {
				var commentId:String = item.id;
				var commentCounter:int = 0;
				for(var i:int=0;i<response.length;i++) {
					var timeStamp:Number = Number(response.getItemAt(i).time)*1000;
					var parentId:String = response.getItemAt(i).parent_id;
					var fromId:String = response.getItemAt(i).fromid;
					var commentDate:Date = new Date();
					commentDate.time = timeStamp;
					var dateDif:int = Math.abs(DateUtils.getHoursDiff(new Date(),commentDate));
					var pageId:String = FlexGlobals.topLevelApplication.mainView.dropdown_pages.selectedItem.id
					if(commentId == parentId && dateDif >= 24 && fromId == pageId) {
						commentCounter++;
					}
				}
				return commentCounter;
			}
			
			function filterUnrepliedComments(item:Object):Boolean {
				if(item.parent_id.toString() == "0" && item.fromid != FlexGlobals.topLevelApplication.mainView.dropdown_pages.selectedItem.id) {
					if(ApplicationModel.commentCountFilter != 0 && ApplicationModel.commentCountFilter == int(item.comment_count)) {
						var latestCommentCount:int = getLatestCommentReplies(item);
						if(latestCommentCount == ApplicationModel.commentCountFilter) {
							return true;
						}
					} else if(ApplicationModel.commentCountFilter == 0  && int(item.comment_count) == ApplicationModel.commentCountFilter) {
						return true;
					}
				}
				return false;
			}
			
			function onPageLoaded(e:Object,res2:*=null):void{
				var data:Array= e as Array;
				
				for each(var obj:Object in data) {
					if(obj.name == "post_impressions") {
						post.postImpressions = obj.values[0].value;
						//dispatchEvent(new Event("POSTS_ARRAY_LOADED"));
					}
				}
				
				if(!data) return;
			}
		}
		
		private function getPageAccessToken(pageId:String):String {
			for each(var page:Object in facebookPagesObjects) {
				if(page.id == pageId) {
					return page.access_token;
				}
			}
			return accessToken;
		}
		
		private var likedCommentsCounter:int = 0;
		
		private function replaceAll(str:String, fnd:String, rpl:String):String{
			return str.split(fnd).join(rpl);
		}
		
		public function postReplyToComment(pageId:String,postId:String,message:String,image_url:String,likeActualComment:Boolean = false):void {
			FlexGlobals.topLevelApplication.processing = true;
			likedCommentsCounter = 0;
			if(commentsLimit == 0) {
				Alert.show("Comment posting limit exceeds!");
				return;
			}
			var params:Object = {};
			params.message = message;
			params.access_token = getPageAccessToken(pageId);
			if(image_url) {
				params.attachment_url = image_url;
			}
			
			var parentCommentId:String = ApplicationModel.selectedCommentIds.length>0?ApplicationModel.selectedCommentIds.getItemAt(0).id.toString():postId.toString();
			var parentCommentorName:String = "";
			if(ApplicationModel.selectedCommentIds.length>0) {
				params.message = replaceAll(message,"<name>",ApplicationModel.selectedCommentIds.getItemAt(0).commentorName);
			/*}
			if(ApplicationModel.selectedCommentIds.length > 0) {*/
				ApplicationModel.facebookCenter.getSourceNameById(ApplicationModel.selectedCommentIds.getItemAt(0).fromid,function(response:Object/*,fail:Object*/):void {
					if(response) {
						response = JSON.parse(response.target.data);
						var str:String = "";
						try {
							if(ApplicationModel.currentIndex == ApplicationModel.selectedReplies.length-1) {
								ApplicationModel.currentIndex = 0;
							}
							str = ApplicationModel.selectedReplies.getItemAt(ApplicationModel.currentIndex) as String;
							ApplicationModel.currentIndex++;
							if(str.search('<name>')!= -1){
								str = str.replace("<name>",response.name);
							}
						} catch(e:Error) {
							str = "";
						}
						message = message.replace("<name>",response.name);
						params.message = str+" "+message;
						trace(params.message)
					}
					FacebookDesktop.api("/"+parentCommentId+"/comments",postReplyResult,params,"POST");
				});
				ApplicationModel.selectedCommentIds.removeItemAt(0);
				ApplicationModel.selectedCommentIds.refresh();
			}else{
				FacebookDesktop.api("/"+parentCommentId+"/comments",postReplyResult,params,"POST");
			}
			
			if(likeActualComment) {
				if(likedCommentsCounter < ApplicationModel.LIKED_COMMENTS_LIMIT) {
					likeComment(parentCommentId,pageId);
					likedCommentsCounter++;
				}
			}
			function postReplyResult(success:Object, fail:Object):void {
				if(success) {
					commentsLimit--;
					FlexGlobals.topLevelApplication.mainView.lblTotalCommentsCount.text = int(FlexGlobals.topLevelApplication.mainView.lblTotalCommentsCount.text)+1;
					if(ApplicationModel.currentIndex == ApplicationModel.selectedReplies.length){
						ApplicationModel.currentIndex = 0 ;
					}
					
					if(ApplicationModel.selectedCommentIds.length > 0) {
						var parentCommentId:String = ApplicationModel.selectedCommentIds.length>0?ApplicationModel.selectedCommentIds.getItemAt(0).id.toString():postId.toString();
						var parentCommentorName:String = ApplicationModel.selectedCommentIds.getItemAt(0).commentorName;
						message = replaceAll(message,"<name>",parentCommentorName);
						ApplicationModel.facebookCenter.getSourceNameById(ApplicationModel.selectedCommentIds.getItemAt(0).fromid,function(response:Object/*,fail:Object*/):void {
							if(response) {
								response = JSON.parse(response.target.data);
								parentCommentorName = response.name;
								var str:String = "";
								try {
									str = ApplicationModel.selectedReplies.getItemAt(ApplicationModel.currentIndex) as String;
									ApplicationModel.currentIndex++;
									if(ApplicationModel.currentIndex == ApplicationModel.selectedReplies.length) {
										ApplicationModel.currentIndex = 0;
									}
									if(str.search('<name>')!= -1) {
										str = str.replace("<name>",response.name);
									}
								} catch(e:Error) {
									str = "";
								}
								message = message.replace("<name>",response.name);
								params.message = str+" "+message;
							}
							FacebookDesktop.api("/"+parentCommentId+"/comments",postReplyResult,params,"POST");
						});
						
						if(ApplicationModel.selectedCommentIds.length>0) {
							ApplicationModel.selectedCommentIds.removeItemAt(0);
							ApplicationModel.selectedCommentIds.refresh();
						}
						//FacebookDesktop.api("/"+parentCommentId+"/comments",postReplyResult,params,"POST");	
						if(likeActualComment) {
							if(likedCommentsCounter < ApplicationModel.LIKED_COMMENTS_LIMIT) {
								likeComment(parentCommentId,pageId);
								likedCommentsCounter++;
							}
						}
					} else {
						FlexGlobals.topLevelApplication.processing = true;
						commentsLimit = ApplicationModel.batchLimit;
						dispatchEvent(new DataEvent("COMMENT_REPLY_ADDED",true,false,postId));
					}
				} else {
					FlexGlobals.topLevelApplication.processing = false;
					Alert.show("Error Replying Comment!","Information");
				}
			}
		}
		
		private function likeComment(parentCommentId:String,pageId:String):void {
			var params:Object = {};
			params.access_token = getPageAccessToken(pageId);
			FacebookDesktop.api(parentCommentId+"/likes",likeCommentResult,params,"POST");
		}
		
		private var cCounter:int = 0;
		
		public function likeCommentsWithoutReply(pageId:String):void {
			if(ApplicationModel.selectedCommentIds.length > 0) {
				var parentCommentId:String = ApplicationModel.selectedCommentIds.getItemAt(cCounter).id.toString();
				var params:Object = {};
				params.access_token = getPageAccessToken(pageId);
				FacebookDesktop.api(parentCommentId+"/likes",commentLikeResponse,params,"POST");
				
				function commentLikeResponse(success:Object,fail:Object):void {
					if(cCounter < ApplicationModel.selectedCommentIds.length-1) {
						cCounter++;
						likeCommentsWithoutReply(pageId);
					} else {
						cCounter = 0;
						dispatchEvent(new Event("COMMENTS_LIKED_SUCESSFULLY",true));
					}
				} 
			}
		}
		
		private function likeCommentResult(success:Object, fail:Object):void {
			if(success) {
			}
		}
		
		/*public function postImageToPageWall(pPageId:String, pText:String, pFile:FileReference, pAccessToken:String):void {
			var params:Object = new Object();
			
			var apiMethod:String = ( pPageId + "/photos");
			
			params.message 	= pText;
			params.source	= pFile;
			params.access_token = pAccessToken;
			
			FacebookDesktop.api( apiMethod, postImageToPageWallResult, params, "POST");
		}
		 
		public function postImageToPageWallResult(success:Object, fail:Object):void {
			if(success) {
				trace("Publish image to fanPage success ");
				dispatchEvent(new DataEvent("WALL_IMAGE_POST_SENT",true,false,success.id));
			} else {
				trace("Publish image to fanPage failed");
				dispatchEvent(new Event("WALL_IMAGE_POST_SEND_FAILED"));	
			}
		}*/
		
		var fanPageCount:int = 1;
		
		public function getUserPagesCallResult(success:Object, fail:Object):void {
			if(success) {
				facebookPagesObjects = success as Array;
				
				if(facebookPagesObjects.length > 0) {
					populateFanPageThumbnail();	
				} else {
					userPagesFaultHandler();
				}
				//dispatchEvent(new Event("PAGES_ARRAY_LOADED"));
				trace("get status success ");
			} else {
				userPagesFaultHandler();
			}
		}
		
		private function userPagesFaultHandler():void {
			FlexGlobals.topLevelApplication.processing = FlexGlobals.topLevelApplication.showProcessingPanel = false;
			var popup:FacebookDataFaultPopup = new FacebookDataFaultPopup();
			PopUpManager.addPopUp(popup,FlexGlobals.topLevelApplication as DisplayObject,true);
			PopUpManager.centerPopUp(popup);
		}
		
		private function populateFanPageThumbnail():void {
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			urlLoader.addEventListener(Event.COMPLETE,completeHnd);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR,function(event:Event):void {
				urlLoader.removeEventListener(Event.COMPLETE,completeHnd);
				facebookPagesObjects[fanPageCount-1].thumb = null; 
				if(fanPageCount < facebookPagesObjects.length) {
					fanPageCount++;
					populateFanPageThumbnail();
				} else {
					dispatchEvent(new Event("PAGES_ARRAY_LOADED"));
					return;
				}
			});
			urlLoader.load(new URLRequest('https://graph.facebook.com/'+facebookPagesObjects[fanPageCount-1].id+'/picture'))	
			
			function completeHnd(event:Event):void {
				urlLoader.removeEventListener(Event.COMPLETE,completeHnd);
				facebookPagesObjects[fanPageCount-1].thumb = event.target.data; 
				if(fanPageCount < facebookPagesObjects.length) {
					fanPageCount++;
					populateFanPageThumbnail();
				} else {
					dispatchEvent(new Event("PAGES_ARRAY_LOADED"));
					return;
				}
			}
		}	
	
		public function logout():void {
			FacebookDesktop.logout(logOutOfFacebookResult);
		}
		
		public function logoutApp():void {
			FacebookDesktop.logout(logOutOfFacebookAppResult);
		}
		
		public function logOutOfFacebookAppResult(success:Boolean):void {
			if(success)
				dispatchEvent(new Event("LOG_OUT_SUCCESS"));
			else
				trace("Could not log out");
		}
		
		public function logOutOfFacebookResult(success:Boolean):void {
			if(success)
				FacebookDesktop.login(logInFacebookResult, PERMISSIONS);
			else
				trace("Could not log out");
		}
		
		public function testFunction():void {
			var params:Object = {};
			params.action_token = this.accessToken;		
			
		}
		
		private function convertToSqlTimeStamp(dateTime:Date):String {
			var df:DateFormatter = new DateFormatter();
			df.formatString = "YYYY-MM-DD JJ:NN:SS";
			return df.format(dateTime);
		}
		
		private var _refreshPosts:Boolean = false;
		
		public function get refreshPosts():Boolean {
			return _refreshPosts;
		}
		
		/*public function checkLatestComment(postId:String):void {
			var urlLoader:URLLoader = new URLLoader();
			var query:String;
			urlLoader.addEventListener(Event.COMPLETE,function(event:Event):void {
				var result:Object = JSON.parse(event.target.data);
				var pResponse:Array = result.data;	
				if(pResponse.length > 0) {
					var startTime:int = int(pResponse[0].time);
					var dateTime:Date = QuickDateFormatter.getDateFromUnixTimeStamp(startTime);
					var date72HoursBack:Date = DateUtils.addDays(dateTime,-3);
					var endTime:int = QuickDateFormatter.getUnixStampTimeFromDate(date72HoursBack);
					ApplicationModel.batchedLastCommentTime = endTime;
					query = "http://graph.facebook.com/fql?q=SELECT%20time,fromid,object_id,id,parent_id,text,comment_count,fromid%20FROM%20comment%20WHERE%20post_id='"+postId+"'%20AND%20time<"+startTime+"%20AND%20time>"+endTime+"%20limit%2010000";
					var urlLoader:URLLoader = new URLLoader();
					urlLoader.addEventListener(Event.COMPLETE,getAllCommentsResultHandler);
					urlLoader.load(new URLRequest(query));
				}
			});
			query = "http://graph.facebook.com/fql?q=SELECT%20time,id%20FROM%20comment%20WHERE%20post_id='"+postId+"'%20order%20by%20time%20desc%20limit%201";
			urlLoader.load(new URLRequest(query));
		}*/
		public function getAllComments(postId:String,refreshPosts:Boolean = false):void {
			_refreshPosts = refreshPosts;
			var query:String;
			if(ApplicationModel.batchModelForCommentsLoading) {
				/*if(ApplicationModel.batchedLastCommentTime == 0) {
					checkLatestComment(postId);
				} else {
					var startTime:int = ApplicationModel.batchedLastCommentTime;
					var dateTime:Date = QuickDateFormatter.getDateFromUnixTimeStamp(startTime);
					var date72HoursBack:Date = DateUtils.addDays(dateTime,-3);
					var endTime:int = QuickDateFormatter.getUnixStampTimeFromDate(date72HoursBack);
					ApplicationModel.batchedLastCommentTime = endTime;
					query = "http://graph.facebook.com/fql?q=SELECT%20time,fromid,object_id,id,parent_id,text,comment_count,fromid%20FROM%20comment%20WHERE%20post_id='"+postId+"'%20AND%20time>="+startTime+"%20AND%20time<="+endTime+"%20limit%2010000";
					var urlLoader:URLLoader = new URLLoader();
					urlLoader.addEventListener(Event.COMPLETE,getAllCommentsResultHandler);
					urlLoader.load(new URLRequest(query));
				}*/
				query = "http://graph.facebook.com/fql?q=SELECT%20time,fromid,object_id,id,parent_id,text,comment_count,fromid%20FROM%20comment%20WHERE%20post_id='"+postId+"'%20AND%20parent_id=0%20order%20by%20time%20desc%20limit%2020%20offset%20"+ApplicationModel.batchCommentOffset;
				ApplicationModel.batchCommentOffset += ApplicationModel.BATCH_COMMENTS_PAGE_SIZE;
				var urlLoader:URLLoader = new URLLoader();
				urlLoader.addEventListener(Event.COMPLETE,getAllCommentsResultHandler);
				urlLoader.load(new URLRequest(query));
			} else {
				query = "http://graph.facebook.com/fql?q=SELECT%20time,fromid,object_id,id,parent_id,text,comment_count,fromid%20FROM%20comment%20WHERE%20post_id='"+postId+"'%20limit%2010000";
				var urlLoader:URLLoader = new URLLoader();
				urlLoader.addEventListener(Event.COMPLETE,getAllCommentsResultHandler);
				urlLoader.load(new URLRequest(query));
			}
		}
		
		public function getSourceNameById(id:String,callback:Function):void {
			/*FacebookDesktop.api("/"+id,callback);*/
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE,callback);
			urlLoader.load(new URLRequest("https://graph.facebook.com/"+id+"?access_token="+this.accessToken));
		}
		
		public function isFanOfPage(id:String,pageId:String,callback:Function):void {
			/*FacebookDesktop.api("/"+id,callback);*/
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE,callback);
			urlLoader.load(new URLRequest("https://graph.facebook.com/"+id+"/likes/"+pageId+"?access_token="+this.accessToken));
		}
		
		private function getAllCommentsResultHandler(event:Event/*, pSomething:Object*/):void {
			(event.target as URLLoader).removeEventListener(Event.COMPLETE,getAllCommentsResultHandler);
			if (!event) {
				Alert.show("There has been an error fecthing data from Facebook");
				FlexGlobals.topLevelApplication.processing = false;
				return;
			}
			
			var result:Object = JSON.parse(event.target.data);
			
			var pResponse:Array = result.data;
			
			var actualComments:ArrayCollection = new ArrayCollection(pResponse);
			actualComments.filterFunction = getParentComments;
			actualComments.refresh();
			
			var totalCommentsFound:String = actualComments.length.toString();
			
			function getParentComments(item:Object):Boolean {
				if(item.parent_id.toString() == "0") {
					return true;
				}
				return false;
			}
			
			var orignalComments:ArrayCollection = new ArrayCollection(pResponse);
			orignalComments.filterFunction = filteroriginalComments;
			orignalComments.refresh();
			pResponse = orignalComments.toArray();
			
			function filteroriginalComments(item:Object):Boolean {
				if(item.parent_id.toString() == "0"  && item.fromid.toString() != FlexGlobals.topLevelApplication.mainView.dropdown_pages.selectedItem.id) {
					if(ApplicationModel.commentCountFilter != 0 && ApplicationModel.commentCountFilter == int(item.comment_count)) {
						var latestCommentCount:int = getLatestCommentReplies(item.id);
						if(latestCommentCount == ApplicationModel.commentCountFilter) {
							return true;
						}
					} else if(ApplicationModel.commentCountFilter == 0 && int(item.comment_count) == ApplicationModel.commentCountFilter) {
						return true;
					}
				}
				return false;
			}
			
			function getLatestCommentReplies(commentId:String):int {
				var commentCounter:int = 0;
				var response:ArrayCollection = new ArrayCollection(pResponse);
				for(var i:int=0;i<response.length;i++) {
					var timeStamp:Number = Number(response.getItemAt(i).time)*1000;
					var parentId:String = response.getItemAt(i).parent_id;
					var fromid:String = response.getItemAt(i).fromid;
					var commentDate:Date = new Date();
					commentDate.time = timeStamp;
					var dateDif:int = Math.abs(DateUtils.getHoursDiff(new Date(),commentDate));
					var pageId:String = FlexGlobals.topLevelApplication.mainView.dropdown_pages.selectedItem.id
					if(commentId == parentId && dateDif >= 24 && fromid == pageId) {
						commentCounter++;
					}
				}
				return commentCounter;
			}
			
			if(pResponse.length < 1 && FlexGlobals.topLevelApplication.mainView.tab.selectedIndex == 2) {
				FlexGlobals.topLevelApplication.processing = false;
				Alert.show("No comments found against selected post","Information");
				return;
			}
			
			commentsArray = new ArrayCollection(pResponse as Array);
			for(var i:int=0;i<commentsArray.length;i++) {
				commentsArray.getItemAt(i).selected = false;
			}
			commentsArray.refresh();
			
			var dataSortField:SortField = new SortField();
			dataSortField.name = 'time';
			dataSortField.numeric = true;
			dataSortField.descending = true;
			var numericDataSort:Sort = new Sort();
			numericDataSort.fields = [dataSortField];
			commentsArray.sort = numericDataSort;
			commentsArray.refresh();
			var commentCount:int = 1;
			
			if(!ApplicationModel.enableCommentsOwnerThumbnail && commentsArray.length > 0) {
				populateCommentOwnerThumbnail();
			} else {
				dispatchEvent(new DataEvent("COMMENTS_ARRAY_LOADED",true,false,totalCommentsFound));
			}
			
			function populateCommentOwnerThumbnail():void {
				var urlLoader:URLLoader = new URLLoader();
				urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
				urlLoader.addEventListener(Event.COMPLETE,completeHnd);
				urlLoader.load(new URLRequest('https://graph.facebook.com/'+commentsArray[commentCount-1].fromid+'/picture'))	
				
				function completeHnd(event:Event):void {
					urlLoader.removeEventListener(Event.COMPLETE,completeHnd);
					commentsArray[commentCount-1].thumb = event.target.data; 
					if(commentCount < commentsArray.length) {
						commentCount++;
						populateCommentOwnerThumbnail();
					} else {
						dispatchEvent(new DataEvent("COMMENTS_ARRAY_LOADED",true,false,totalCommentsFound));
						return;
					}
				}
			}	
		}
		
		public static function getUnixStampTime(pDate:Date):int {
			var utc:Number = Math.round(pDate.time/1000);
			return utc;
		}
		
		public function searchPublicPosts(keyword:String):void {
			var params:Object = {};
			params.q = keyword;
			params.type = 'post';
			FacebookDesktop.api("/search",onSearchResult,params);
			
			function onSearchResult(success:Object,fail:Object):void {
				if(success) {
					searchPostsResults = new ArrayCollection(success as Array);
					searchPostsResults.refresh();
					dispatchEvent(new Event("SEARCH_RESULTS_LOADED"));
				}
			}
		}
	}
}